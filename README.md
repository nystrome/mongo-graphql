# Steps to running solely in docker

1. `./gradlew bootBuildImage`
1. `docker network create mongo`
1. `docker run -d -p 27017:27017 --rm --network=mongo --name mongo mongo`
1. `docker run -d -p 8080:8080 --rm --network=mongo mongo-graphql:0.0.1 mongo-graphql`