package io.nystrome.resolvers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import graphql.kickstart.tools.GraphQLMutationResolver;
import io.nystrome.models.Customer;
import io.nystrome.repository.CustomerRepository;

@Component
public class MutationResolver implements GraphQLMutationResolver {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer createCustomer(String firstName, String lastName) {
        return customerRepository.save(
			Customer.builder()
			.firstName(firstName)
			.lastName(lastName)
			.build()
		);
    }
    
}
