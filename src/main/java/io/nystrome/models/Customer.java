package io.nystrome.models;

import org.springframework.data.annotation.Id;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Customer {
    
    @Id
    public String id;
    public final String firstName;
    public final String lastName;
    public final Address address;
    
}
