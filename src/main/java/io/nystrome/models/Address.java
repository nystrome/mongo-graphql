package io.nystrome.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Address {
    public final String line1;
    public final String line2;
    public final String postcode;
}
